import { calculateBonuses } from "./bonus-system.js"

describe('Bonus system testing', () => {
    let app

    const Standard = 'Standard'
    const Premium = 'Premium'
    const Diamond = 'Diamond'
    const Error = 'Someting other'

    const low = 5000
    const small = 10000
    const mediumBound = 40000
    const medium = 50000
    const bigBound = 90000
    const big = 100000

    test('Standard low',  (done) => {
        expect(calculateBonuses(Standard, low)).toEqual(0.05);
        done();
    });

    test('Standard small',  (done) => {
        expect(calculateBonuses(Standard, small)).toEqual(0.05 * 1.5);
        done();
    });

    test('Standard medium bound',  (done) => {
        expect(calculateBonuses(Standard, mediumBound)).toEqual(0.05 * 1.5);
        done();
    });

    test('Standard medium',  (done) => {
        expect(calculateBonuses(Standard, medium)).toEqual(0.05 * 2);
        done();
    });

    test('Standard big bind',  (done) => {
        expect(calculateBonuses(Standard, bigBound)).toEqual(0.05 * 2);
        done();
    });

    test('Standard big',  (done) => {
        expect(calculateBonuses(Standard, big)).toEqual(0.05 * 2.5);
        done();
    });

    test('Premium low',  (done) => {
        expect(calculateBonuses(Premium, low)).toEqual(0.1);
        done();
    });

    test('Premium small',  (done) => {
        expect(calculateBonuses(Premium, small)).toEqual(0.1 * 1.5);
        done();
    });

    test('Premium medium bound',  (done) => {
        expect(calculateBonuses(Premium, mediumBound)).toEqual(0.1 * 1.5);
        done();
    });

    test('Premium medium',  (done) => {
        expect(calculateBonuses(Premium, medium)).toEqual(0.1 * 2);
        done();
    });

    test('Premium big bound',  (done) => {
        expect(calculateBonuses(Premium, bigBound)).toEqual(0.1 * 2);
        done();
    });

    test('Premium big',  (done) => {
        expect(calculateBonuses(Premium, big)).toEqual(0.1 * 2.5);
        done();
    });

    test('Diamond low',  (done) => {
        expect(calculateBonuses(Diamond, low)).toEqual(0.2);
        done();
    });

    test('Diamond small',  (done) => {
        expect(calculateBonuses(Diamond, small)).toEqual(0.2 * 1.5);
        done();
    });

    test('Diamond medium bound',  (done) => {
        expect(calculateBonuses(Diamond, mediumBound)).toEqual(0.2 * 1.5);
        done();
    });

    test('Diamond medium',  (done) => {
        expect(calculateBonuses(Diamond, medium)).toEqual(0.2 * 2);
        done();
    });

    test('Diamond big bound',  (done) => {
        expect(calculateBonuses(Diamond, bigBound)).toEqual(0.2 * 2);
        done();
    });

    test('Diamond big',  (done) => {
        expect(calculateBonuses(Diamond, big)).toEqual(0.2 * 2.5);
        done();
    });

    test('Error low',  (done) => {
        expect(calculateBonuses(Error, low)).toEqual(0);
        done();
    });

    test('Error small',  (done) => {
        expect(calculateBonuses(Error, small)).toEqual(0);
        done();
    });

    test('Error medium',  (done) => {
        expect(calculateBonuses(Error, medium)).toEqual(0);
        done();
    });

    test('Error big',  (done) => {
        expect(calculateBonuses(Error, big)).toEqual(0);
        done();
    });

})